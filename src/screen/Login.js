import React, { useEffect, useState } from 'react';
import {
   View,
   Text,
   TouchableOpacity,
   TextInput,
   Platform,
   KeyboardAvoidingView,
   StyleSheet,
   Alert
} from 'react-native';
import NotificationManager from '../utils/NotificationManager';
import {useNavigation} from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'

const Login = ({

}) => {
   const navigation = useNavigation();
   const {data, token} = NotificationManager();
   const [email, setEmail] = useState('')
   const [password, setPassword] = useState('')
   const [loading, setLoading] = useState(true)
   const [fmctoken, setfcmtoken] = useState('')

   const actionLogin = async () => {
      if(email === 'admin' && password === 'admin123') {
         try {
            console.log("fmctoken",fmctoken)
            await AsyncStorage.setItem('tokenFCM', `${token.data}`)
         }catch(error){
            console.log('setitem didnt work')
            alert('setitem didnt work')
         }
         navigation.replace('Home')
      } else {
         Alert.alert('Ooopss...', 'Email atau password salah !')
      }
   }

   const checkStatusLogin = async () => {
      const token = await AsyncStorage.getItem('tokenFCM');
      if(token) {
         navigation.navigate('Home');
      } else {
         setLoading(false);
      }
   }

   useEffect(() => {
      checkStatusLogin();
   },[])
   
   if(loading) {
      return <View />
   } else {
      return(
         <KeyboardAvoidingView
            enabled
            behavior='padding'
            keyboardVerticalOffset={Platform.OS === 'android' ? -600 : 60}
            style={{
               flex: 1,
               paddingHorizontal: 16,
               justifyContent: 'center',
               alignItems: 'center'
            }}
         >
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>Login screen</Text>
            <TextInput
               placeholder='Masukkan email'
               value={email}
               onChangeText={(text) => setEmail(text)}
               style={styles.txtInput}
            />
            <TextInput
               placeholder='Masukkan password'
               value={password}
               secureTextEntry={true}
               onChangeText={(text) => setPassword(text)}
               style={styles.txtInput}
            />
            <TouchableOpacity
               activeOpacity={0.8}
               style={[styles.btnLogin, {
                  backgroundColor: email === '' && password === '' ? '#757575' : '#ff5722'
               }]}
               disabled={email === '' && password === ''}
               onPress={actionLogin}
            >
               <Text style={{fontSize: 16, color: '#fff', fontWeight: '700'}}>Login</Text>
            </TouchableOpacity>
         </KeyboardAvoidingView>
      )
   }
}

const styles = StyleSheet.create({
   btnLogin: {
      width: 200,
      paddingVertical: 10,
      borderRadius: 6,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
   },
   txtInput: {
      width: '100%',
      borderRadius: 6,
      padding: 10,
      borderColor: '#dedede',
      borderWidth: 1,
      marginTop: 20
   },
   container: {
      flex: 1, 
      backgroundColor: '#fff',
      // justifyContent: 'center',
      // alignItems: 'center'
   }
})

export default Login;