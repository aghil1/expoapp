import React, {useEffect, useState} from 'react';
import {
   View,
   Text,
   TouchableOpacity,
   ToastAndroid,
   StyleSheet
} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import * as Clipboard from 'expo-clipboard'

const Home = ({}) => {
   const [tokenfcm, setTokenFcm] = useState('')
   
   const getToken = async () => {
      const token = await AsyncStorage.getItem('tokenFCM');
      setTokenFcm(token)
   }

   const copyToClipboard = async () => {
      await Clipboard.setStringAsync(tokenfcm);
      ToastAndroid.show('Token telah disalin', ToastAndroid.SHORT);
   };

   useEffect(() => {
      getToken();
   },[])

   return(
      <View style={styles.container}>
         <Text style={{fontSize: 14, fontWeight: 'bold'}}>{tokenfcm}</Text>
         <TouchableOpacity
            onPress={copyToClipboard}
            style={styles.btnCopy}
         >
            <Text style={{color: '#fff', fontWeight: '600'}}>Salin</Text>
         </TouchableOpacity>
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff'
   },
   btnCopy: {
      width: 130,
      paddingVertical: 10,
      borderRadius: 6,
      backgroundColor: '#43a047',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
   }
})

export default Home;